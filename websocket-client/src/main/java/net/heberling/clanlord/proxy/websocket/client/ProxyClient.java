package net.heberling.clanlord.proxy.websocket.client;

import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

public class ProxyClient {
    private static final byte TYPE_UDP = 0;
    private static final byte TYPE_TCP = 1;

    private static final class Client extends WebSocketClient {

        private final Socket socket;

        private final DatagramSocket datagramSocket;

        private SocketAddress clientUDP;

        public Client(URI serverUri, Socket socket, DatagramSocket datagramSocket) {
            super(serverUri);
            this.socket = socket;
            this.datagramSocket = datagramSocket;
        }

        @Override
        public void close() {
            if (datagramSocket != null) {
                datagramSocket.close();
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            super.close();
        }

        @Override
        public void onOpen(ServerHandshake handshakedata) {
            new Thread(
                            () -> {
                                try {
                                    byte[] buf = new byte[0xFFFF];
                                    InputStream inputStream = socket.getInputStream();
                                    while (socket.isConnected() && !socket.isClosed()) {
                                        int read = inputStream.read(buf);
                                        if (read > 0) {
                                            byte[] sendBuf = new byte[read + 1];
                                            sendBuf[0] = TYPE_TCP;
                                            System.arraycopy(buf, 0, sendBuf, 1, read);
                                            send(sendBuf);
                                        } else {
                                            // closed
                                            break;
                                        }
                                    }
                                    close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    close(500, e.getMessage());
                                }
                            })
                    .start();
            new Thread(
                            () -> {
                                byte[] buf = new byte[0xFFFF];
                                while (!datagramSocket.isClosed()) {
                                    DatagramPacket dp = new DatagramPacket(buf, buf.length);

                                    try {
                                        datagramSocket.receive(dp);

                                        clientUDP = dp.getSocketAddress();

                                        byte[] sendBuf = new byte[dp.getLength() + 1];
                                        sendBuf[0] = TYPE_UDP;
                                        System.arraycopy(buf, 0, sendBuf, 1, dp.getLength());
                                        send(sendBuf);

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        close(500, e.getMessage());
                                    }
                                }
                            })
                    .start();
        }

        @Override
        public void onMessage(String message) {}

        @Override
        public void onMessage(ByteBuffer bytes) {
            byte type = bytes.get();
            byte[] msg = new byte[bytes.remaining()];
            bytes.get(msg);
            if (type == TYPE_TCP) {
                try {
                    socket.getOutputStream().write(msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (type == TYPE_UDP) {
                try {
                    DatagramPacket packet = new DatagramPacket(msg, msg.length, clientUDP);
                    datagramSocket.send(packet);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onClose(int code, String reason, boolean remote) {}

        @Override
        public void onError(Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        ServerSocket serverSocket = new ServerSocket(5010);
        Socket socket = serverSocket.accept();
        DatagramSocket datagramSocket = new DatagramSocket(5010);
        Client client = new Client(new URI("wss://clp.tisoft.de/proxy"), socket, datagramSocket);
        client.connect();
    }
}
