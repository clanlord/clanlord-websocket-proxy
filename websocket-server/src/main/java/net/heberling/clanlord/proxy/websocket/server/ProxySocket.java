package net.heberling.clanlord.proxy.websocket.server;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.datagram.DatagramSocket;
import io.vertx.core.net.NetSocket;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ServerEndpoint("/proxy")
@ApplicationScoped
public class ProxySocket {
    private static final byte TYPE_UDP = 0;
    private static final byte TYPE_TCP = 1;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProxySocket.class);

    @Inject Vertx vertx;

    private final Map<Session, Connection> connections = new HashMap<>();

    private static final class Connection {
        private final NetSocket socket;
        private final DatagramSocket datagramSocket;

        private Connection(NetSocket socket, DatagramSocket datagramSocket) {
            this.socket = socket;
            this.datagramSocket = datagramSocket;
        }
    }

    @OnOpen
    public void onOpen(Session session) {
        LOGGER.info("Opening session {}", session.getId());
        vertx.createNetClient()
                .connect(
                        5010,
                        "server.deltatao.com",
                        res -> {
                            if (res.succeeded()) {
                                LOGGER.info("Connected session {}", session.getId());
                                NetSocket socket = res.result();
                                socket.closeHandler(
                                        event -> {
                                            if (session.isOpen()) {
                                                try {
                                                    session.close(
                                                            new CloseReason(
                                                                    CloseReason.CloseCodes
                                                                            .NORMAL_CLOSURE,
                                                                    "Server closed connection"));
                                                } catch (IOException e) {
                                                    LOGGER.error(
                                                            "Failed closing session {}",
                                                            session.getId(),
                                                            e);
                                                }
                                            }
                                        });
                                socket.handler(
                                        event -> {
                                            byte[] bytes = event.getBytes();
                                            sendMessage(bytes, session, TYPE_TCP);
                                        });
                                DatagramSocket datagramSocket = vertx.createDatagramSocket();
                                datagramSocket.handler(
                                        event -> {
                                            byte[] bytes = event.data().getBytes();
                                            sendMessage(bytes, session, TYPE_UDP);
                                        });
                                connections.put(session, new Connection(socket, datagramSocket));

                            } else {
                                LOGGER.error(
                                        "Failed to connect session {}",
                                        session.getId(),
                                        res.cause());
                            }
                        });
    }

    private void sendMessage(byte[] bytes, Session session, byte type) {
        LOGGER.info("Sending message for session {} with type {}", session.getId(), type);

        ByteBuffer byteBuffer = ByteBuffer.allocate(bytes.length + 1);
        byteBuffer.put(type);
        byteBuffer.put(bytes);
        byteBuffer.rewind();
        session.getAsyncRemote().sendBinary(byteBuffer);
    }

    @OnClose
    public void onClose(Session session) {
        LOGGER.info("Closing session {}", session.getId());
        Connection connection = connections.remove(session);
        connection.datagramSocket.close();
        connection.socket.close();
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        LOGGER.info("Closing session with error {}", session.getId(), throwable);
        Connection connection = connections.remove(session);
        connection.datagramSocket.close();
        connection.socket.close();
    }

    @OnMessage
    public void onMessage(Session session, byte[] message) {
        Connection connection = connections.get(session);
        Buffer buffer = Buffer.buffer();
        buffer.appendBytes(message, 1, message.length - 1);
        LOGGER.info("Got message for session {} with type {}", session.getId(), message[0]);
        if (message[0] == TYPE_TCP) {
            connection.socket.write(buffer);
        } else if (message[0] == TYPE_UDP) {
            connection.datagramSocket.sender(5010, "server.deltatao.com").write(buffer);
        }
    }
}
